from django.urls import path
from accounts.views import log_in, user_logout
from receipts.views import receipt_list
from accounts.views import signup


urlpatterns = [
    path("signup/", signup, name="signup"),
    path("logout/", user_logout, name="logout"),
    path("", receipt_list, name="home"),
    path("login/", log_in, name="login"),
]
