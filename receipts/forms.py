from django import forms
from receipts.models import Receipt


class ReceiptForm(forms.ModelForm):
    class meta:
        model = Receipt
        fields = [
            "vendor",
            "total",
            "tax",
            "date",
            "catagory",
            "account",
        ]
